Expire Entity Queues
=================

This module is a plugin for Cache Expiration that allows you to invalidate cache
of entity queue pages when an entity queue gets updated, or when nodes are
added/removed from an entity queue.

It is closely based on Expire Nodequeues.