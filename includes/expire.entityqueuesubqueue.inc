<?php

/**
 * @file
 * Provides class that expires ExpireEntityqueueSubqueue.
 */

/**
 * Class ExpireEntityqueueSubqueue.
 */
class ExpireEntityqueueSubqueue implements ExpireInterface {

  /**
   * Executes expiration actions for entityqueues.
   *
   * @param object $display
   *   The entityqueue object.
   * @param int $action
   *   Action that has been executed.
   * @param bool $skip_action_check
   *   Shows whether should we check executed action or just expire entityqueue.
   */
  public function expire($display, $action, $skip_action_check = FALSE) {
    $enabled_actions = variable_get('expire_entityqueues_actions', array());
    $enabled_actions = array_filter($enabled_actions);

    // Stop further expiration if executed action is not selected by admin.
    if (!in_array($action, $enabled_actions) && !$skip_action_check) {
      return;
    }

    $expire_urls = array();

    // Expire front page.
    $expire_front_page = variable_get('expire_entityqueues_front_page', EXPIRE_ENTITYQUEUES_FRONT_PAGE);
    if ($expire_front_page) {
      $expire_urls = ExpireAPI::getFrontPageUrls();
    }

    // Expire entity page.
    $expire_entity_page = variable_get('expire_entityqueues_entity_page', EXPIRE_ENTITYQUEUES_ENTITY_PAGE);
    if ($expire_entity_page && !empty($display->type)  && !empty($display->eid)) {
      $expire_urls[$display->type . '-' . $display->eid] = $display->type . '/' . $display->eid;
    }

    // Expire custom pages.
    $expire_custom = variable_get('expire_entityqueues_custom', EXPIRE_ENTITYQUEUES_CUSTOM);

    if ($expire_custom) {
      $pages = variable_get('expire_entityqueues_custom_pages');

      $urls = ExpireAPI::expireCustomPages($pages, array()); //TODO : Look into second arg, token_options
      $expire_urls = array_merge($expire_urls, $urls);
    }

    // Flush page cache for expired urls.
    ExpireAPI::executeExpiration($expire_urls, 'entityqueues', $display);
  }

}
