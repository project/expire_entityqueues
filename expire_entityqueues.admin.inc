<?php

/**
 * @file
 * Admin settings.
 */

/**
 * Entity Queues expiration settings.
 */
function expire_entityqueues_admin_settings_form_elements(&$form, &$form_state, $form_id) {
  $form['tabs']['entityqueues'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity Queues expiration'),
    '#group' => 'tabs',
    '#weight' => 8,
  );

  $form['tabs']['entityqueues']['actions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity Queues actions'),
  );

  $form['tabs']['entityqueues']['actions']['expire_entityqueues_actions'] = array(
    '#type' => 'checkboxes',
    '#description' => t('Page cache for entity queues will be flushed after selected actions.'),
    '#options' => array(
      EXPIRE_ENTITYQUEUES_ADD_ENTITY => t('Node added in a an entity queue'),
      EXPIRE_ENTITYQUEUES_REMOVE_ENTITY => t('Node removed from a an entity queue'),
      EXPIRE_ENTITYQUEUES_CHANGE_ORDER => t('Node order changed in a an entity queue'),
      EXPIRE_ENTITYQUEUES_ENTITY_UPDATE => t('Node appearing in an entity queue is updated'),
    ),
    '#default_value' => variable_get('expire_entityqueues_actions', array()),
  );

  $form['tabs']['entityqueues']['expire'] = array(
    '#type' => 'fieldset',
    '#title' => t('What URLs should be expired when an entityqueues action is triggered?'),
  );

  $form['tabs']['entityqueues']['expire']['expire_entityqueues_front_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Front page'),
    '#description' => t('Expire url of the site front page'),
    '#default_value' => variable_get('expire_entityqueues_front_page', EXPIRE_ENTITYQUEUES_FRONT_PAGE),
  );

  $form['tabs']['entityqueues']['expire']['expire_entityqueues_entity_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Entity page'),
    '#description' => t('Expire url of the added/removed entity page.'),
    '#default_value' => variable_get('expire_entityqueues_entity_page', EXPIRE_ENTITYQUEUES_ENTITY_PAGE),
  );

  $form['tabs']['entityqueues']['expire']['expire_entityqueues_custom'] = array(
    '#type' => 'checkbox',
    '#title' => t('Custom pages'),
    '#description' => t('Expire user-defined custom urls.'),
    '#default_value' => variable_get('expire_entityqueues_custom', EXPIRE_ENTITYQUEUES_CUSTOM),
  );

  // @codingStandardsIgnoreStart
  $form['tabs']['entityqueues']['expire']['expire_entityqueues_custom_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter custom URLs'),
    '#description' => t('Enter one relative URL per line. It can be the path of an entity (e.g. !example1) or of any alias (e.g. !example2). However, it has to be the final URL, not a redirect (use the !link1 and !link2 modules).', array(
        '!example1' => '<strong>user/[user:uid]</strong>',
        '!example2' => '<strong>my/path</strong>',
        '!link1' => l('Global Redirect', 'https://drupal.org/project/globalredirect'),
        '!link2' => l('Redirect', 'https://drupal.org/project/redirect')
      )) . '<br/>'
      . t('If you want to match a path with any ending, add "|wildcard" to the end of the line (see !link1 for details). Example: !example1 will match !example1a, but also !example1b, !example1c, etc.', array(
        '!link1' => l('function cache_clear_all', 'https://api.drupal.org/api/drupal/includes%21cache.inc/function/cache_clear_all/7'),
        '!example1' => '<strong>my/path|wildcard</strong>',
        '!example1a' => '<strong>my/path</strong>',
        '!example1b' => '<strong>my/path/one</strong>',
        '!example1c' => '<strong>my/path/two</strong>'
      )) . '<br/>'
      . t('You may use tokens here.'),
    '#states' => array(
      'visible' => array(
        ':input[name="expire_entityqueues_custom"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => variable_get('expire_entityqueues_custom_pages'),
  );
  // @codingStandardsIgnoreEnd
}
