<?php

/**
 * @file
 * Flush cache of nodequeue related pages.
 */

module_load_include('inc', 'expire_entityqueues', 'expire_entityqueues.admin');

// Nodequeues actions.(TODO Note, not yet detected)
define('EXPIRE_ENTITYQUEUES_ADD_ENTITY', 1);
define('EXPIRE_ENTITYQUEUES_REMOVE_ENTITY', 2);
define('EXPIRE_ENTITYQUEUES_CHANGE_ORDER', 3);
define('EXPIRE_ENTITYQUEUES_ENTITY_UPDATE', 4);

// Default values.
define('EXPIRE_ENTITYQUEUES_FRONT_PAGE', FALSE);
define('EXPIRE_ENTITYQUEUES_ENTITY_PAGE', FALSE);
define('EXPIRE_ENTITYQUEUES_CUSTOM', FALSE);

/**
 * Implements hook_help().
 */
function expire_entityqueues_help($path, $arg) {
  switch ($path) {
    case 'admin/structure/entityqueue':
      $output = '<p>';
      $output .= t("If you need to purge the cache when an entity queue is updated, go to the") . ' ';
      $output .= l('Cache Expiration administration page', 'admin/config/system/expire');
      $output .= ' ' . t('. Select the Entityqueues tab and fill the form to get the desired behavior.');
      $output .= '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function expire_entityqueues_form_expire_admin_settings_form_alter(&$form, &$form_state, $form_id) {
  expire_entityqueues_admin_settings_form_elements($form, $form_state, $form_id);
}

/**
 * Implements hook_entity_update().
 */
function expire_entityqueues_entity_update($entity, $type) {

  // Handle queue updates
  if ($type == 'entityqueue_subqueue') {
    expire_entityqueues_handle_entityqueue_updates($entity, $type);
  }
  else if ($type == 'file' || $type == 'node' || $type == 'term') {
    // Check if this entity is in any queues.
    $queues = entityqueue_queue_load_by_target_type($type);
    expire_entityqueues_expire_multiple_queues($entity, $queues);
  }

}

/**
 * @param $entity
 * @param $type
 */
function expire_entityqueues_handle_entityqueue_updates($entity, $type) {
  list($sq_id) = entity_extract_ids($type, $entity);
  $action = EXPIRE_ENTITYQUEUES_ADD_ENTITY; // TODO: Determine type of update (add, remove, re-order), set constant

  // TODO: Build $queue object (with optional $nid)
  $display = new stdClass();
  $display->type = NULL;
  $display->sqid = (int) $sq_id;
  $display->eid = (int) 0; //TODO, sometimes need a $nid

  expire_entityqueues_execute_expiration($display, $action);
}

/**
 * @param $entity
 * @param $queues
 */
function expire_entityqueues_expire_multiple_queues($entity, $queues) {
  if (!empty($queues)) {
    $eid = $entity_type = null;
    if (isset($entity->nid)) {
      $eid = $entity->nid;
      $entity_type = 'node';
    }
    else if (isset($entity->tid)) {
      $eid = $entity->tid;
      $entity_type = 'term';
    }
    else if (isset($entity->fid)) {
      $eid = $entity->fid;
      $entity_type = 'file';
    }

    foreach ($queues as $queue_name => $queue) {
      $sub_queues = entityqueue_subqueue_load_multiple( FALSE, array('queue' => $queue_name));
      if (!empty($sub_queues)) {
        foreach ($sub_queues as $sq_id => $sub_queue) {
          $position = expire_entityqueues_get_entity_position($sub_queue, $eid);
          if (!is_null($position)) {
            // Expire paths for this entity queue.

            $expire_display = new stdClass();
            $expire_display->type = $entity_type;
            $expire_display->sqid = (int) $sq_id;
            $expire_display->eid = (int) $eid;

            expire_entityqueues_execute_expiration($expire_display, EXPIRE_ENTITYQUEUES_ENTITY_UPDATE);
          }
        }
      }
    }
  }
}

/**
 * Execute expiration method for object.
 */
function expire_entityqueues_execute_expiration($object, $action) {
  $status = variable_get('expire_status', EXPIRE_STATUS_DISABLED);

  if ($status) {
    if ($handler = _expire_get_expiration_handler('entityqueue_subqueue')) {
      $handler->expire($object, $action);
    }
  }
  return FALSE;
}

/**
 * Get the position of a node in a subqueue, or 0 if not found.
 */
function expire_entityqueues_get_entity_position($sub_queue, $eid){
  $position = NULL;
  $entities = $sub_queue->eq_node[LANGUAGE_NONE]; //TODO: Test with multilingual
  foreach ($entities as $pos => $entity_entry) {
    if ($entity_entry['target_id'] == $eid) {
      return ($position = $pos);
    }
  }
}
